#
#  Be sure to run `pod spec lint DRSalesforceInterface.podspec' to ensure this is a
#  valid spec and to remove all comments including this before submitting the spec.
#
#  To learn more about Podspec attributes see http://docs.cocoapods.org/specification.html
#  To see working Podspecs in the CocoaPods repo see https://github.com/CocoaPods/Specs/
#

Pod::Spec.new do |s|


  s.name         = "DRSalesforceInterface"
  s.version      = "0.2.3"
  s.summary      = "A library for bulding native iOS apps that interact with the Salesforce platform."
  s.description  = "A lightweight library that wraps the Salesforce Mobile SDK for iOS, written in Objective-C."
  s.homepage     = "https://bitbucket.org/nickPl/drsalesforceinterface"
  s.license      = 'MIT'
  s.author       = { "nikolas89" => "nikolasborman@rambler.ru" }


  s.platform     = :ios, '9.0'
  s.source       = { :git => "https://nickPl@bitbucket.org/nickPl/drsalesforceinterface.git", :tag => s.version.to_s }


  s.source_files  = "*{h,m}"
  s.requires_arc = true

  s.dependency 'SalesforceSDKCore'
  s.dependency 'SmartStore'
  s.dependency 'SmartSync'
  s.dependency 'DRskpsmtpmessage'
  s.dependency 'Bolts'

end
