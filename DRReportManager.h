//
//  ReportManager.h
//  ReviewApp
//
//  Created by Plumb on 4/15/16.
//  Copyright © 2016 Recapify. All rights reserved.
//

#import <Foundation/Foundation.h>

/*!
 *  DRReportManager is the class used to compose reports
 */
@interface DRReportManager : NSObject

/*!
 *  Stores the share URL of the file
 */
@property (strong, nonatomic) NSString *recapFileShareUrl;

/*!
 *  Array containing emails of contacts that exist in user Salesforce account
 */
@property (strong, nonatomic) NSMutableArray *existingContactsEmails;

/*!
 *  Array containing first names of contacts that exist in user Salesforce account
 */
@property (strong, nonatomic) NSMutableArray *existingContactsFirstNames;

/*!
 *  Array containing last names of contacts that exist in user Salesforce account
 */
@property (strong, nonatomic) NSMutableArray *existingContactsLastNames;

/*!
 *  Array containing emails of contacts that not exist in user Salesforce account
 */
@property (strong, nonatomic) NSMutableArray *nonexistentContactsEmails;

/*!
 *  Array containing first names of contacts that not exist in user Salesforce account
 */
@property (strong, nonatomic) NSMutableArray *nonexistentContactsFirstNames;

/*!
 *  Array containing last names of contacts that not exist in user Salesforce account
 */
@property (strong, nonatomic) NSMutableArray *nonexistentContactsLastNames;

/*!
 *  The user's name
 */
@property (strong, nonatomic) NSString *accountName;

/*!
 *  The user's email
 */
@property (strong, nonatomic) NSString *currenUserMail;

/*!
 *  Array containing Id of the created tasks
 */
@property (strong, nonatomic) NSMutableDictionary *usersTasksId;

/*!
 *  The name of the updated opportunity
 */
@property (strong, nonatomic) NSString *updatedOpportunity;

/*!
 *  The meeting title
 */
@property (strong, nonatomic) NSString *meetingTitle;

/*!
 *  The date of the meeting
 */
@property (strong, nonatomic) NSDate *meetingStartDate;

/*!
 *  Array of Dictionaries, each containing a title, id and email keys
 */
@property (strong, nonatomic) NSArray *createdOpenTasks;

/*!
 *  Create singleton
 *  @discussion Add '#import "DRReportManager.h"'
 *  @param Without parameters.
 *  @return Unique singleton object.
 *  @code [DRReportManager sharedManager]
 *  @endcode
 */
+ (DRReportManager *)sharedManager;

/*!
 *  Reset report manager
 *  @param Without parameters.
 */
- (void)resetManager;

/*!
 *  Compose chatter report
 *
 *  @return Dictionary of segments for chatter report
 */
- (NSDictionary *)getChatterPostParameters;

/*!
 *  Compose email report
 *
 *  @return Composed email message
 */
- (NSString *)getMailMessage;

@end 
