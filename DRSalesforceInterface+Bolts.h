//
//  DRSalesforceInterface+Bolts.h
//  Recapify
//
//  Created by Plumb on 5/4/16.
//  Copyright © 2016 Paul Svetlichny. All rights reserved.
//

#import <DRSalesforceInterface/DRSalesforceInterface.h>

@class BFTask;

@interface DRSalesforceInterface (Bolts)

/*!
 *  Sends a request that can upload a new file to the Salesforce server
 *
 *  @param fileData    The Data to upload to the server
 *  @param name        The name/title of the file
 *  @param description A description of the file
 *
 *  @return An instance of 'BFTask'. On successful execution, 'task.result' will contain an instance of NSString with file identifier
 */
- (BFTask *)postPDFFile:(NSData *)fileData
               withName:(NSString *)name
            description:(NSString *)description;

/*!
 *  Sends a request to the Salesforce server to get share link of the given file
 *
 *  @param fileId The file identifier
 *  @return An instance of 'BFTask'. On successful execution, 'task.result' will contain an instance of NSString with file share link
 */
- (BFTask *)getShareLinkForFile:(NSString *)fileId;

/*!
 *  Sends a request to the Salesforce server to add task to the given opportunity
 *
 *  @param task          NSDictionary containing field names and values for the task
 *  @param opportunityId The opportunity identifier
 *
 *  @return An instance of 'BFTask'. On successful execution, 'task.result' will contain an instance of NSDictionary
 */
- (BFTask *)postTask:(NSDictionary *)task toOpportunity:(NSString *)opportunityId;

/*!
 *  Sends a request to the Salesforce server to get contacts with the given emails
 *
 *  @param emails The list of emails
 *
 *  @return An instance of 'BFTask'. On successful execution, 'task.result' will contain an instance of NSArray with contacts
 */
- (BFTask *)getContactsWithEmails:(NSArray *)emails;

/*!
 *  Sends a request to the Salesforce server to add new tasks related to the given contacts
 *
 *  @param task     NSDictionary containing field names and values for the task
 *  @param contacts NSArray containing contacts identifiers
 *
 *  @return An instance of 'BFTask'. On successful execution, 'task.result' will contain an instance of NSDictionary with contacts identifier
 */
- (BFTask *)postTask:(NSDictionary *)task forContacts:(NSArray *)contacts;

/*!
 *  Sends a request to the Salesforce server to add new open [Not Started] task to the given contact
 *
 *  @param task    NSDictionary containing field names and values for the task
 *  @param contact NSDictionary containing field names and values for the contact
 *
 *  @return An instance of 'BFTask'. On successful execution, 'task.result' will contain an instance of NSDictionary with task id, title and contact email
 */
- (BFTask *)postOpenTask:(NSDictionary *)task forContact:(NSDictionary *)contact;

/*!
 *  Sends a request to the Salesforce server to add new open [Not Started] task assigned to the current user
 *
 *  @param task NSDictionary containing field names and values for the task
 *
 *  @return An instance of 'BFTask'. On successful execution, 'task.result' will contain an instance of NSDictionary with task id, title and contact email
 */
- (BFTask *)postOpenTaskForUserAcc:(NSDictionary *)task;

/*!
 *  Sends a request to the Salesforce server to post file to the chatter feed
 *
 *  @param fileId The file identifier
 *
 *  @return An instance of 'BFTask'
 */
- (BFTask *)postFileToChatter:(NSString *)fileId;

/*!
 *  Sends a request to the Salesforce server to add new chatter post
 *
 *  @param queryParams The parameters of the request
 *
 *  @return An instance of 'BFTask'
 */
- (BFTask *)postChatterFeedWithQueryParameters:(NSDictionary *)queryParams;

/*!
 *  Sends a request to the Salesforce server to check if sharing via link is enabled
 *
 *  @return An instance of 'BFTask'
 */
- (BFTask *)checkSharingViaLinkPermission;

@end
