//
//  ReportManager.m
//  ReviewApp
//
//  Created by Plumb on 4/15/16.
//  Copyright © 2016 Recapify. All rights reserved.
//

#import "DRReportManager.h"
#import "DRSalesforceInterface.h"

@interface DRReportManager ()

@property (strong, nonatomic) NSString *recapifyLink;
@property (strong, nonatomic) NSString *createNewContactBaseLink;

@end

@implementation DRReportManager

#pragma mark - Singleton

+ (DRReportManager *)sharedManager
{
    static DRReportManager *manager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[DRReportManager alloc] init];
    });
    
    return manager;
}

#pragma mark - Lifecycle

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        [self resetManager];
    }
    
    return self;
}

- (void)resetManager
{
    self.existingContactsEmails = [[NSMutableArray alloc]init];
    self.existingContactsFirstNames = [[NSMutableArray alloc]init];
    self.existingContactsLastNames = [[NSMutableArray alloc]init];
    self.nonexistentContactsEmails = [[NSMutableArray alloc]init];
    self.nonexistentContactsFirstNames = [[NSMutableArray alloc]init];
    self.nonexistentContactsLastNames = [[NSMutableArray alloc]init];
    self.recapifyLink = @"www.recapify.com";
    self.createNewContactBaseLink = @"https://eu6.salesforce.com/003/e?retURL=%2F003%2Fo";
    self.recapFileShareUrl = nil;
    self.updatedOpportunity = nil;
    self.meetingTitle = nil;
}

#pragma mark - Custom Accessors

- (NSString *)meetingTitle
{
    if (!_meetingTitle)
    {
        return @"[No Meeting Title Found Placeholder]";
    }
    else
    {
        return _meetingTitle;
    }
}

- (NSString *)currenUserMail
{
    if (!_currenUserMail)
    {
        return [[DRSalesforceInterface sharedInstance] getUserMail];
    }
    else
    {
        return _currenUserMail;
    }
}

#pragma mark - Private

- (NSString *)formatDate:(NSDate *)theDate
{
    if(theDate)
    {
        static NSDateFormatter *formatter = nil;
        if (!formatter)
        {
            formatter = [[NSDateFormatter alloc]init];
            [formatter setDateStyle:NSDateFormatterMediumStyle];
            [formatter setTimeStyle:NSDateFormatterShortStyle];
        }
        return [formatter stringFromDate:theDate];
    }
    else
    {
        return @"No Meeting Date Found Placeholder";
    }
}

#pragma mark - Chatter Post

- (NSDictionary *)getChatterPostParameters
{
    NSMutableDictionary * postParams = [NSMutableDictionary new];
    NSMutableArray * segmentArray = [NSMutableArray new];
    
    if (self.updatedOpportunity)
    {
        [segmentArray addObject:[self opportunitySegment]];
    }
    
    [segmentArray addObject:[self chattetPostTitleSegment]];
    
    if (self.existingContactsFirstNames && [self.existingContactsFirstNames count] > 0)
    {
        [segmentArray addObject:[self chattetPostTaskTitleSegment]];
        for (NSInteger i = 0; i < [self.existingContactsEmails count]; i++)
        {
            NSDictionary *taskDict = [self assignedTaskForContactWithFirstName:self.existingContactsFirstNames[i]
                                                                   andLastName:self.existingContactsLastNames[i]];
            for (id task in self.createdOpenTasks)
            {
                if (![task isMemberOfClass:[NSError class]])
                {
                    if ([task[@"email"] caseInsensitiveCompare:self.existingContactsEmails[i]] == NSOrderedSame)
                    {
                        NSLog(@"IF");
                        NSLog(@"\n[%@ %@] was assigned the activity [%@]",self.existingContactsFirstNames[i], self.existingContactsLastNames[i], task[@"title"]);
                        
                        NSMutableDictionary *segmentDict = [[NSMutableDictionary alloc] init];
                        segmentDict[@"type"] = @"Text";
                        segmentDict[@"text"] = [NSString stringWithFormat:@"\n[%@ %@] was assigned the activity [%@]",self.existingContactsFirstNames[i], self.existingContactsLastNames[i], task[@"title"]];
                        [segmentArray addObject:segmentDict];
                    }
                }
            }
            [segmentArray addObject:taskDict];
        }
    }
    
    if(self.nonexistentContactsFirstNames && [self.nonexistentContactsFirstNames count] > 0)
    {
        [segmentArray addObject:[self notFoundPersonTitleSegment]];
        for (NSInteger i = 0; i < [self.nonexistentContactsFirstNames count]; i++)
        {
            NSDictionary *taskDict = [self notFoundContactWithFirstName:self.nonexistentContactsFirstNames[i]
                                                               lastName:self.nonexistentContactsLastNames[i]
                                                               andEmail:self.nonexistentContactsEmails[i]];
            [segmentArray addObject:taskDict];
        }
    }
    
    postParams[@"body"] = @{@"messageSegments": segmentArray};
    if ([DRReportManager sharedManager].recapFileShareUrl)
    {
       postParams[@"attachment"] = [self getAttachmentDictionary];
    }
    
    return postParams;
}

- (NSDictionary *)chattetPostTitleSegment
{
    NSMutableDictionary *segmentDict = [[NSMutableDictionary alloc] init];
    segmentDict[@"type"] = @"Text";
    segmentDict[@"text"] = [NSString stringWithFormat:@"\n\n The meeting Recap for event [%@] has been added", self.meetingTitle];
    return segmentDict;
}

- (NSDictionary *)chattetPostTaskTitleSegment
{
    NSMutableDictionary *segmentDict = [[NSMutableDictionary alloc] init];
    segmentDict[@"type"] = @"Text";
    segmentDict[@"text"] = @"\n\nThe following activities were assigned";
    return segmentDict;
}

- (NSDictionary *)assignedTaskForContactWithFirstName:(NSString *)firstName andLastName:(NSString *)lastName
{
    NSMutableDictionary *segmentDict = [[NSMutableDictionary alloc] init];
    segmentDict[@"type"] = @"Text";
    segmentDict[@"text"] = [NSString stringWithFormat:@"\n[%@ %@] was assigned the activity [The meeting Recap for event %@ has been added]",firstName, lastName, self.meetingTitle];
    return segmentDict;
}

- (NSDictionary *)notFoundPersonTitleSegment
{
    NSMutableDictionary *segmentDict = [[NSMutableDictionary alloc] init];
    segmentDict[@"type"] = @"Text";
    segmentDict[@"text"] = [NSString stringWithFormat:@"\n\nThe following contacts were not found under the [%@] Account and should be added", self.accountName];
    return segmentDict;
}

- (NSDictionary *)notFoundContactWithFirstName:(NSString *)firstName lastName:(NSString *)lastName andEmail:(NSString *)email
{
    NSMutableDictionary *segmentDict = [[NSMutableDictionary alloc] init];
    segmentDict[@"type"] = @"Text";
    segmentDict[@"text"] = [NSString stringWithFormat:@"\n [%@ %@] with email [%@]", firstName, lastName, email];
    return segmentDict;
}

- (NSDictionary *)opportunitySegment
{
    NSString *text;
    if ([DRReportManager sharedManager].recapFileShareUrl)
    {
        if (self.updatedOpportunity)
        {
            text = [NSString stringWithFormat:@"\n\n The opportunity %@ has been updated and a copy of the meeting Recap has been added.", self.updatedOpportunity];
        }
        else
        {
            text = @"\n\n The copy of the meeting Recap has been added:";
        }
    }
    else
    {
        if (self.updatedOpportunity)
        {
            text = [NSString stringWithFormat:@"\n\n The opportunity %@ has been updated.", self.updatedOpportunity];
        }
        else
        {
            text = @"";
        }
        
    }
    
    NSMutableDictionary *segmentDict = [[NSMutableDictionary alloc] init];
    segmentDict[@"type"] = @"Text";
    segmentDict[@"text"] = text;
    
    return segmentDict;
}

- (NSDictionary *)getAttachmentDictionary
{
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    dict[@"url"] = [DRReportManager sharedManager].recapFileShareUrl;
    dict[@"urlName"] = [NSString stringWithFormat:@"Recap link for event [%@]", self.meetingTitle];
    
    return dict;
}

#pragma mark - Mail Message

- (NSString *)getMailMessage
{
    NSString *recapLinkTextSegment;
    if (self.recapFileShareUrl)
    {
        recapLinkTextSegment = [NSString stringWithFormat:@"<li>Click [<a href=\"%@\">Here</a>] for the meeting Recap</li>", self.recapFileShareUrl];
    }
    else
    {
        recapLinkTextSegment = @"";
    }
    
    NSString *mailMessage = [NSString stringWithFormat:@"<p>This email includes all the SFDC updates for the meeting titled %@ on [%@]. "
                             "The following actions were automated by <a href=\"%@\">Recapify</a>."
                             
                             "%@" //[self recapString]
                             
                             "%@" //recap share link
                             
                             "%@" //[self assignedTasks]
                             
                             
                             "%@" //[self notFoundContacts]
                             
                             "%@"
                             
                             "</ul>"
                             //"%@" // recap share link
                             
                             "<p>Thank you for using Recapify to provide real time updates to your management, team and systems within minutes of leaving your meeting. See more at www.Recapify.com.</p>",self.meetingTitle, [self formatDate:self.meetingStartDate], self.recapifyLink, [self recapString], recapLinkTextSegment, [self assignedTasks], [self notFoundContacts], [self opportunityString]];
    return mailMessage;
}

- (NSString *)recapString
{
    NSLog(@"");
    NSString *text;
    if (self.updatedOpportunity)
    {
        text = [NSString stringWithFormat:@"</p><ul><li>Recapify added this Recap to the [%@] Account to the opportunity [%@]: </li>", self.accountName, self.updatedOpportunity];
    }
    else
    {
        text = [NSString stringWithFormat:@"</p><ul><li>Recapify added this Recap to the [%@] Account. </li>", self.accountName];
    }
    
    return text;
}


- (NSString *)assignedTasks
{
    if ([self.existingContactsEmails count] > 0)
    {
        NSMutableString *resultedString = [[NSMutableString alloc]init];
        NSString *titleString = @"<li>The following activities were assigned</li><ul>";
        [resultedString appendString:titleString];
        NSString *taskTitle = [NSString stringWithFormat:@"The meeting Recap for event %@ has been added", self.meetingTitle];
        
        for (NSInteger i = 0; i < [self.existingContactsEmails count]; i++)
        {
            for (id task in self.createdOpenTasks)
            {
                if (![task isMemberOfClass:[NSError class]])
                {
                    if ([task[@"email"] caseInsensitiveCompare:self.existingContactsEmails[i]] == NSOrderedSame)
                    {
                        NSString *str = [self assignedTask:task[@"title"]
                                                    withId:task[@"id"]
                                    toContactWithFirstName:self.existingContactsFirstNames[i]
                                                  lastName:self.existingContactsLastNames[i]
                                                  andEmail:self.existingContactsEmails[i]];
                        [resultedString appendString:str];
                    }
                }
            }
            
            NSString *str = [self assignedTask:taskTitle
                                        withId:[self.usersTasksId objectForKey:self.existingContactsEmails[i]]
                        toContactWithFirstName:self.existingContactsFirstNames[i]
                                      lastName:self.existingContactsLastNames[i]
                                      andEmail:self.existingContactsEmails[i]];
            [resultedString appendString:str];
        }
        [resultedString appendString:@"</ul>"];
        
        return resultedString;
    }
    else
    {
        return @"";
    }
}

- (NSString *)assignedTask:(NSString *)taskTitle withId:(NSString *)taskId toContactWithFirstName:(NSString *)firstName lastName:(NSString *)lastName andEmail:(NSString *)email
{
    NSString *baseTaskUrl = @"https://eu6.salesforce.com/";
    NSString *fullTaskLink = [NSString stringWithFormat:@"%@%@", baseTaskUrl, taskId];
    
    NSString *taskLinkSegment = [NSString stringWithFormat:@"<a href=\"%@\">%@</a>", fullTaskLink, taskTitle];
    NSString *assignedString = [NSString stringWithFormat:@"<li>[%@ %@] was assigned the activity [%@]</li>", firstName, lastName, taskLinkSegment];
    
    return assignedString;
}

- (NSString *)assignedTask:(NSString *)taskTitle toContactWithFirstName:(NSString *)firstName andLastName:(NSString *)lastName
{
    NSString *baseTaskUrl = @"https://eu6.salesforce.com/";
    NSString *fullTaskLink = [NSString stringWithFormat:@"%@", baseTaskUrl];
    NSString *taskLinkSegment = [NSString stringWithFormat:@"<a href=\"%@\">%@</a>", fullTaskLink, taskTitle];
    NSString *assignedString = [NSString stringWithFormat:@"<li>[%@ %@] was assigned the task [%@]</li>", firstName, lastName, taskLinkSegment];
    
    return assignedString;
}

- (NSString *)notFoundContacts
{
    if ([self.nonexistentContactsFirstNames count] > 0)
    {
        NSMutableString *resultedString = [[NSMutableString alloc]init];
        NSString *titleString = [NSString stringWithFormat:@"<li>The following contacts were not found and should be added</li><ul>"];
        [resultedString appendString:titleString];
        
        for (NSInteger i = 0; i < [self.nonexistentContactsFirstNames count]; i++)
        {
            NSString *str = [self notFoundContactWithFirstName:self.nonexistentContactsFirstNames[i] andLastName:self.nonexistentContactsLastNames[i] andEmail:self.nonexistentContactsEmails[i]];
            [resultedString appendString:str];
        }
        [resultedString appendString:@"</ul>"];
        
        return resultedString;
    }
    else
    {
        return @"";
    }
}

- (NSString *)opportunityString
{
    NSString *text;
    if (self.updatedOpportunity)
    {
        //text = [NSString stringWithFormat:@"<li>The opportunity [%@] has been updated</li>", self.updatedOpportunity];
        text = @"";
    }
    else
    {
        text = @"";
    }
    
    return text;
}

- (NSString *)notFoundContactWithFirstName:(NSString *)firstName andLastName:(NSString *)lastName andEmail:(NSString *)email
{
    NSString *notFoundString = [NSString stringWithFormat:@"<li> %@ %@ [<a href=\"%@\">Create New Contact</a>] [<a href=\"%@\">Search For Contact</a>]</li>", firstName, lastName, [self generateLinkForCreatingNewContactWithFirstName:firstName andLastName:lastName andEmail:email], [self generateLinkForSearchingContactWithFirstName:firstName andLastName:lastName andEmail:email]];
    return notFoundString;
}

- (NSString *)generateLinkForCreatingNewContactWithFirstName:(NSString *)firstName andLastName:(NSString *)lastName andEmail:(NSString *)email
{
    NSString *createNewContactBaseLink = @"https://eu6.salesforce.com/003/e?retURL=%2F003%2Fo";
    
    NSString *fullName = [NSString stringWithFormat:@"%@ %@", firstName, lastName];
    NSArray *nameParts = [fullName componentsSeparatedByString:@" "];
    
    NSString *firstContactName;
    NSString *lastContactName;
    NSString *fullNameLink;
    
    if ([nameParts count] == 2)
    {
        firstContactName = nameParts[0];
        lastContactName = nameParts[1];
        fullNameLink = [NSString stringWithFormat:@"%@&name_firstcon2=%@&name_lastcon2=%@", createNewContactBaseLink, firstContactName, lastContactName];
    }
    else
    {
        firstContactName = [nameParts componentsJoinedByString:@" "];
        fullNameLink = [NSString stringWithFormat:@"%@&name_firstcon2=%@", createNewContactBaseLink, firstContactName];
    }
    
    if (![email isEqualToString:@"No email address"])
    {
        fullNameLink = [NSString stringWithFormat:@"%@&con15=%@", fullNameLink, email];
    }

    return fullNameLink;
}

- (NSString *)generateLinkForSearchingContactWithFirstName:(NSString *)firstName andLastName:(NSString *)lastName andEmail:(NSString *)email
{
    NSString *searchContactBaseLink = @"https://eu5.salesforce.com/_ui/search/ui/UnifiedSearchResults?searchType=2&str=#!/fen=003&initialViewMode=detail&str=";
    NSString *fullName = [NSString stringWithFormat:@"%@ %@", firstName, lastName];
    
    NSString *fullRequest = [NSString stringWithString:fullName];
    
    if (![email isEqualToString:@"No email address"])
    {
        fullRequest = [NSString stringWithFormat:@"%@ %@", fullRequest, email];
    }
    
    return [NSString stringWithFormat:@"%@%@", searchContactBaseLink, fullRequest];
}

@end
