//
//  DRSalesforceInterface+Bolts.m
//  Recapify
//
//  Created by Plumb on 5/4/16.
//  Copyright © 2016 Paul Svetlichny. All rights reserved.
//

#import "DRSalesforceInterface+Bolts.h"
#import <Bolts/Bolts.h>
#import <SalesforceSDKCore/SFRestAPI.h>
#import <SalesforceSDKCore/SFRestRequest.h>
#import <SalesforceSDKCore/SFUserAccountManager.h>
#import <SalesforceSDKCore/SFIdentityData.h>
#import <SalesforceSDKCore/SFRestAPI+Files.h>
#import <SalesforceSDKCore/SFRestAPI+Blocks.h>


@implementation DRSalesforceInterface (Bolts)

- (BFTask *)postPDFFile:(NSData *)fileData
               withName:(NSString *)name
            description:(NSString *)description
{
    BFTaskCompletionSource *promise = [BFTaskCompletionSource taskCompletionSource];
    
    SFRestRequest *request = [[SFRestAPI sharedInstance] requestForUploadFile:fileData
                                                                         name:name
                                                                  description:description
                                                                     mimeType:@"application/pdf"];
    NSString *path = [NSString stringWithFormat:@"/%@/connect/files/users/me", [SFRestAPI sharedInstance].apiVersion];
    request.path = path;
    
    [[SFRestAPI sharedInstance] sendRESTRequest:request
                                      failBlock:^(NSError *e)
     {
         [promise setError:e];
     }
                                  completeBlock:^(id response)
     {
         NSString *fileId = response[@"id"];
         [promise setResult:fileId];
     }];
    
    return  promise.task;
}

- (BFTask *)getShareLinkForFile:(NSString *)fileId
{
    BFTaskCompletionSource *promise = [BFTaskCompletionSource taskCompletionSource];
    
    SFRestRequest *request = [[SFRestAPI sharedInstance] requestForResources];
    NSString *path = [NSString stringWithFormat:@"/%@/connect/files/%@/file-shares/link", [SFRestAPI sharedInstance].apiVersion, fileId];
    request.path = path;
    request.method = SFRestMethodPUT;
    
    [[SFRestAPI sharedInstance] sendRESTRequest:request
                                      failBlock:^(NSError *e)
     {
         [promise setError:e];
     }
                                  completeBlock:^(id response)
     {
         NSString *shareLink = response[@"fileViewUrl"];
         [promise setResult:shareLink];
     }];
    
    return  promise.task;
}

- (BFTask *)postTask:(NSDictionary *)task toOpportunity:(NSString *)opportunityId
{
    BFTaskCompletionSource *promise = [BFTaskCompletionSource taskCompletionSource];
    
    NSMutableDictionary *taskDict = [task mutableCopy];
    [taskDict setObject:opportunityId forKey:@"WhatId"];
    [[SFRestAPI sharedInstance] performCreateWithObjectType:@"Task"
                                                     fields:taskDict
                                                  failBlock:^(NSError *e)
     {
         [promise setError:e];
     }
                                              completeBlock:^(NSDictionary *dict)
     {
         [promise setResult:dict];
     }];
    
    return  promise.task;
}

- (BFTask *)getContactsWithEmails:(NSArray *)emails
{
    BFTaskCompletionSource *promise = [BFTaskCompletionSource taskCompletionSource];
    
    NSString *emailsString = [self getListString:emails];
    NSString * quesryString = [NSString stringWithFormat:@"SELECT Id, FirstName, LastName, Email FROM Contact WHERE Email IN (%@) ",emailsString];
    SFRestRequest * request = [[SFRestAPI sharedInstance] requestForQuery:quesryString];
    [[SFRestAPI sharedInstance]sendRESTRequest:request failBlock:^(NSError *e)
     {
         [promise setError:e];
     }
                                 completeBlock:^(id response)
     {
         NSArray *records = [response objectForKey:@"records"];
         [promise setResult:records];
     }];
    
    return  promise.task;
}

- (BFTask *)postTask:(NSDictionary *)task forContacts:(NSArray *)contacts
{
    if ([contacts count] == 0)
    {
        return [BFTask taskWithResult:@"No Contacts found. Operation had been finished successfully"];
    }
        
    BFTaskCompletionSource *promise = [BFTaskCompletionSource taskCompletionSource];
    NSMutableDictionary *taskDict = [task mutableCopy];
    __block NSMutableDictionary *resultsDict = [[NSMutableDictionary alloc] init];
    __block NSUInteger counter = 0;
    
    for (NSInteger i = 0; i < [contacts count]; i++)
    {
        NSDictionary *contact = contacts[i];
        taskDict[@"WhoId"] = contact[@"Id"];
        taskDict[@"Status"] = @"Completed";
        NSString *contactEmail = contact[@"Email"];
        
        [[SFRestAPI sharedInstance] performCreateWithObjectType:@"Task"
                                                         fields:taskDict
                                                      failBlock:^(NSError *e)
         {
             [promise setError:e];
         }
                                                  completeBlock:^(NSDictionary *dict)
         {
             counter++;
             [resultsDict setObject:dict[@"id"] forKey:contactEmail];
             
             if (counter >= [contacts count])
             {
                 [promise setResult: resultsDict];
             }
         }];
    }
    
    return  promise.task;
}

- (BFTask *)postOpenTaskForUserAcc:(NSDictionary *)task
{
    BFTaskCompletionSource *promise = [BFTaskCompletionSource taskCompletionSource];
    NSMutableDictionary *taskDict = [[NSMutableDictionary alloc] init];
    
    taskDict[@"WhoId"] = task[@"WhoId"];
    taskDict[@"Subject"] = task[@"Subject"];
    taskDict[@"Status"] = @"Not Started";
    taskDict[@"Description"] = task[@"Description"];
    
    __block NSMutableDictionary *resultsDict = [[NSMutableDictionary alloc] init];
    resultsDict[@"title"] = task[@"title"];
    resultsDict[@"email"] = task[@"email"];
    
    [[SFRestAPI sharedInstance] performCreateWithObjectType:@"Task"
                                                     fields:taskDict
                                                  failBlock:^(NSError *e)
     {
         [promise setError:e];
     }
                                              completeBlock:^(NSDictionary *dict)
     {
         resultsDict[@"id"] = dict[@"id"];
         [promise setResult:resultsDict];
     }];
    
    return  promise.task;
}

- (BFTask *)postOpenTask:(NSDictionary *)task forContact:(NSDictionary *)contact
{
    BFTaskCompletionSource *promise = [BFTaskCompletionSource taskCompletionSource];
    NSMutableDictionary *taskDict = [task mutableCopy];
    
    taskDict[@"WhoId"] = contact[@"Id"];
    taskDict[@"Status"] = @"Not Started";
    taskDict[@"OwnerId"] = contact[@"Id"];
    
    __block NSMutableDictionary *resultsDict = [[NSMutableDictionary alloc] init];
    resultsDict[@"email"] = contact[@"Email"];
    resultsDict[@"WhoId"] = contact[@"Id"];
    resultsDict[@"Subject"] = taskDict[@"Subject"];
    resultsDict[@"title"] = taskDict[@"Subject"];

    if (taskDict[@"Description"])
    {
        resultsDict[@"Description"] = taskDict[@"Description"];
    }
    
    [[SFRestAPI sharedInstance] performCreateWithObjectType:@"Task"
                                                     fields:taskDict
                                                  failBlock:^(NSError *e)
     {
         resultsDict[@"error"] = e.localizedDescription;
         [promise setResult:resultsDict];
     }
                                              completeBlock:^(NSDictionary *dict)
     {
         resultsDict[@"id"] = dict[@"id"];
         [promise setResult:resultsDict];
     }];
    
    return  promise.task;
}

- (BFTask *)postFileToChatter:(NSString *)fileId
{
    BFTaskCompletionSource *promise = [BFTaskCompletionSource taskCompletionSource];
    
    NSMutableDictionary * postParams = [NSMutableDictionary new];
    
    NSMutableDictionary *attDict = [[NSMutableDictionary alloc]init];
    attDict[@"attachmentType"] = @"ExistingContent";
    attDict[@"contentDocumentId"] = fileId;
    
    postParams[@"attachment"] = attDict;
    
    SFUserAccount *user = [[SFUserAccountManager sharedInstance] currentUser];
    SFRestRequest* request = [[SFRestAPI sharedInstance] requestForResources];
    request.path = [NSString stringWithFormat:@"/services/data/v30.0/chatter/feeds/user-profile/%@/feed-items", user.idData.userId];
    request.method = SFRestMethodPOST;
    request.queryParams = postParams;
    
    [[SFRestAPI sharedInstance] sendRESTRequest:request
                                      failBlock:^(NSError *e)
     {
         [promise setError:e];
     }
                                  completeBlock:^(id response)
     {
         [promise setResult:response];
     }];
    
    return  promise.task;
}

- (BFTask *)postChatterFeedWithQueryParameters:(NSDictionary *)queryParams
{
    BFTaskCompletionSource *promise = [BFTaskCompletionSource taskCompletionSource];
    
    SFUserAccount *user = [[SFUserAccountManager sharedInstance] currentUser];
    SFRestRequest* request = [[SFRestAPI sharedInstance] requestForResources];
    request.path = [NSString stringWithFormat:@"/services/data/v24.0/chatter/feeds/user-profile/%@/feed-items", user.idData.userId];
    request.method = SFRestMethodPOST;
    request.queryParams = queryParams;
    
    [[SFRestAPI sharedInstance] sendRESTRequest:request
                                      failBlock:^(NSError *e)
     {
         [promise setError:e];
     }
                                  completeBlock:^(id response)
     {
         [promise setResult:response];
     }];
    
    return  promise.task;
}

- (BFTask *)checkSharingViaLinkPermission
{
    BFTaskCompletionSource *promise = [BFTaskCompletionSource taskCompletionSource];
    
    NSString *query = [NSString stringWithFormat:@"SELECT Name FROM ContentDistribution LIMIT 1"];
    SFRestRequest *request = [[SFRestAPI sharedInstance] requestForQuery:query];
    [[SFRestAPI sharedInstance] sendRESTRequest:request
                                      failBlock:^(NSError *e)
     {
         [promise setError:e];
     }
                                  completeBlock:^(id response)
     {
         NSArray *records = [response objectForKey:@"records"];
         [promise setResult:records];
     }];
    
    return  promise.task;
}

#pragma mark - Helpers

- (NSString *)getListString:(NSArray *)inputs
{
    NSString * result = @"";
    
    if ([inputs count] == 0)
    {
        return @"''";
    }
    else
    {
        for (int i = 0; i < [inputs count]; ++i)
        {
            result = [[[result stringByAppendingString:@"'"]
                       stringByAppendingString: [inputs objectAtIndex:i]]
                      stringByAppendingString:@"',"];
        }
        result = [result substringToIndex:result.length - 1];
    }
    return result;
}

@end
