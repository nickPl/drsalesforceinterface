//
//  DRMailUtility.m
//  ReviewApp
//
//  Created by Plumb on 4/19/16.
//  Copyright © 2016 Recapify. All rights reserved.
//

#import "DRMailUtility.h"
#import "SKPSMTPMessage.h"
#import "NSData+Base64Additions.h"
#import "DRReportManager.h"

@interface DRMailUtility () <SKPSMTPMessageDelegate>

@property (copy, nonatomic) void (^successHandler)(void);
@property (copy, nonatomic) void (^failureHandler)(NSError *error);

@end



@implementation DRMailUtility

#pragma mark - Lifecycle

+ (DRMailUtility *)sharedUtility
{
    static DRMailUtility *utility = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        utility = [[DRMailUtility alloc] init];
    });
    
    return utility;
}

- (void)sendMessage:(NSString *)message
   withMeetingTitle:(NSString *)meetingTitle
            toEmail:(NSString *)email
 withAttachmentName:(NSString *)attachmentName
  andAttachmentData:(NSData *)attachmentData
          onSuccess:(void(^)(void))success
          onFailure:(void(^)(NSError *error))failure
{
    self.successHandler = success;
    self.failureHandler = failure;
    
    SKPSMTPMessage *testMsg = [[SKPSMTPMessage alloc] init];
    
    testMsg.requiresAuth = YES;
    testMsg.fromEmail = @"postmaster@bestfit.mailgun.org";
    testMsg.fromDisplayName = @"Recapify";
    testMsg.login = @"postmaster@bestfit.mailgun.org";
    testMsg.pass = @"3mz-0mgxo945";
    testMsg.toEmail = [DRReportManager sharedManager].currenUserMail;
    testMsg.relayHost = @"smtp.mailgun.org";
    testMsg.wantsSecure = YES;
    testMsg.subject = [NSString stringWithFormat:@"The %@ meeting Recap", meetingTitle];
    testMsg.relayPorts = [[NSArray alloc] initWithObjects:[NSNumber numberWithShort:587], [NSNumber numberWithShort:25], [NSNumber numberWithShort:465], nil];
    testMsg.delegate = self;
    
    NSDictionary *plainPart = [NSDictionary dictionaryWithObjectsAndKeys:
                               @"text/html; charset=UTF-8",kSKPSMTPPartContentTypeKey,
                               message,kSKPSMTPPartMessageKey,
                               @"8bit",kSKPSMTPPartContentTransferEncodingKey,nil];
    NSDictionary *vcfPart;
    if (attachmentData)
    {
        NSString * contentTypeValue = [NSString stringWithFormat:@"text/directory;\r\n\tx-unix-mode=0644;\r\n\tname=\"%@\"", attachmentName];
        NSString * contentDispositionValue = [NSString stringWithFormat:@"attachment;\r\n\tfilename=\"%@\"", attachmentName];
        
        vcfPart = [NSDictionary dictionaryWithObjectsAndKeys:contentTypeValue,kSKPSMTPPartContentTypeKey,
                                                             contentDispositionValue,kSKPSMTPPartContentDispositionKey,
                                                             [attachmentData encodeBase64ForData],kSKPSMTPPartMessageKey,
                                                             @"base64",kSKPSMTPPartContentTransferEncodingKey,
                                                             nil];
    }

    NSMutableArray *partsArray = [[NSMutableArray alloc]init];
    [partsArray addObject:plainPart];
    if (attachmentData)
    {
        [partsArray addObject:vcfPart];
    }
    testMsg.parts = partsArray;
    
    [testMsg send];
}

#pragma mark - SKPSMTPMessageDelegate

- (void)messageSent:(SKPSMTPMessage *)message
{
    self.successHandler();
    
    self.successHandler = nil;
    self.failureHandler = nil;
}

- (void)messageFailed:(SKPSMTPMessage *)message error:(NSError *)error
{
    self.failureHandler(error);
    
    self.successHandler = nil;
    self.failureHandler = nil;
}

@end
