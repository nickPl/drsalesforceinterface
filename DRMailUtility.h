//
//  DRMailUtility.h
//  ReviewApp
//
//  Created by Plumb on 4/19/16.
//  Copyright © 2016 Recapify. All rights reserved.
//

#import <Foundation/Foundation.h>

/*!
 *  Use this class for sending mail
 */
@interface DRMailUtility : NSObject

/*!
 *  Create singleton
 *  @discussion Add '#import "DRMailUtility.h"'
 *  @param Without parameters.
 *  @return Unique singleton object.
 *  @code [DRMailUtility sharedUtility] 
 *  @endcode
 */
+ (DRMailUtility *)sharedUtility;

/*!
 *  Use this method to send email
 *
 *  @param message        The email text
 *  @param meetingTitle   The meeting title
 *  @param email          The reciver email address
 *  @param attachmentName The name/title of the attachment file 
 *  @param attachmentData The data of the attachment
 *  @param success        The block to be executed when the sending process successfully completes
 *  @param failure        The block to be executed when the sending process fails
 */
- (void)sendMessage:(NSString *)message
   withMeetingTitle:(NSString *)meetingTitle
            toEmail:(NSString *)email
 withAttachmentName:(NSString *)attachmentName
  andAttachmentData:(NSData *)attachmentData
          onSuccess:(void(^)(void))success
          onFailure:(void(^)(NSError *error))failure;
@end
