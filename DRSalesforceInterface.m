//
//  DRSalesforceInterface.m
//  DRSalesforceInterface
//
//  Created by Plumb on 3/31/16.
//  Copyright © 2016 Recapify. All rights reserved.
//

#import "DRSalesforceInterface.h"
#import <SalesforceSDKCore/SFRestAPI.h>
#import <SalesforceSDKCore/SFRestRequest.h>
#import <SalesforceSDKCore/SFUserAccountManager.h>
#import <SalesforceSDKCore/SFIdentityData.h>
#import <SalesforceSDKCore/SFRestAPI+Files.h>
#import <SalesforceSDKCore/SFRestAPI+Blocks.h>
#import "DRReportManager.h"

@interface DRSalesforceInterface () <SFRestDelegate>

@end

@implementation DRSalesforceInterface

#pragma mark - Singleton

+ (DRSalesforceInterface *) sharedInstance
{
    static DRSalesforceInterface *instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[DRSalesforceInterface alloc] init];
        SFUserAccount *user = [[SFUserAccountManager sharedInstance] currentUser];
        [DRReportManager sharedManager].accountName = user.fullName;
    });
    
    return instance;
}

#pragma mark - GET methods

- (void)getUserOpportunitiesWithDelegate:(id<DRSalesforceInterfaceDelegate>)delegate
{
    self.delegate = delegate;
    SFUserAccount *user = [[SFUserAccountManager sharedInstance] currentUser];
    NSString *query = [NSString stringWithFormat:@"SELECT Name,ID FROM Opportunity WHERE IsClosed = FALSE AND OwnerId = '%@'", user.idData.userId];
    SFRestRequest *request = [[SFRestAPI sharedInstance] requestForQuery:query];
    [[SFRestAPI sharedInstance] send:request delegate:self];
}

- (void)getUserOpportunitiesOnSuccess:(void(^)(NSArray *opportunities))success
                            onFailure:(void(^)(NSError *error))failure
{
    SFUserAccount *user = [[SFUserAccountManager sharedInstance] currentUser];
    NSString *query = [NSString stringWithFormat:@"SELECT Name,ID FROM Opportunity WHERE IsClosed = FALSE AND OwnerId = '%@'", user.idData.userId];
    SFRestRequest *request = [[SFRestAPI sharedInstance] requestForQuery:query];
    [[SFRestAPI sharedInstance] sendRESTRequest:request
                                      failBlock:^(NSError *e)
     {
         if (failure)
         {
             failure(e);
         }
     }
                                  completeBlock:^(id response)
     {
         NSArray *records = [response objectForKey:@"records"];
         if (success)
         {
             success(records);
         }
     }];
}

- (void)getEmailsFromContactsRelatedToOpportunity:(NSString *)opportunityId
                                         delegate:(id<DRSalesforceInterfaceDelegate>)delegate
{
    self.delegate = delegate;
    
    NSString *query = [NSString stringWithFormat: @"SELECT Email FROM Contact WHERE ID IN (SELECT ContactId FROM OpportunityContactRole WHERE OpportunityId = '%@')", opportunityId];
    SFRestRequest *request = [[SFRestAPI sharedInstance] requestForQuery:query];
    [[SFRestAPI sharedInstance] send:request delegate:self];
}

- (void)getEmailsFromContactsRelatedToOpportunity:(NSString *)opportunityId
                                        onSuccess:(void(^)(NSArray *emails))success
                                        onFailure:(void(^)(NSError *error))failure
{
    NSString *query = [NSString stringWithFormat: @"SELECT Email FROM Contact WHERE ID IN (SELECT ContactId FROM OpportunityContactRole WHERE OpportunityId = '%@')", opportunityId];
    SFRestRequest *request = [[SFRestAPI sharedInstance] requestForQuery:query];
    [[SFRestAPI sharedInstance] sendRESTRequest:request
                                      failBlock:^(NSError *e)
     {
         if (failure)
         {
             failure(e);
         }
     }
                                  completeBlock:^(id response)
     {
         NSArray *records = [response objectForKey:@"records"];
         if (success)
         {
             success(records);
         }
     }];
}

- (void)getContactWithEmail:(NSString *)email
                   delegate:(id<DRSalesforceInterfaceDelegate>)delegate
{
    self.delegate = delegate;
    NSString *query = [NSString stringWithFormat:@"SELECT Id, Name FROM Contact WHERE Email = '%@'", email];
    SFRestRequest *request = [[SFRestAPI sharedInstance] requestForQuery:query];
    [[SFRestAPI sharedInstance] send:request delegate:self];
}

- (void)getContactWithEmail:(NSString *)email
                  onSuccess:(void(^)(NSArray *contacts))success
                  onFailure:(void(^)(NSError *error))failure
{
    NSString *query = [NSString stringWithFormat:@"SELECT Id, Name FROM Contact WHERE Email = '%@'", email];
    SFRestRequest *request = [[SFRestAPI sharedInstance] requestForQuery:query];
    [[SFRestAPI sharedInstance] sendRESTRequest:request
                                      failBlock:^(NSError *e)
     {
         if (failure)
         {
             failure(e);
         }
     }
                                  completeBlock:^(id response)
     {
         NSArray *records = [response objectForKey:@"records"];
         if (success)
         {
             success(records);
         }
     }];
}

- (void)getFolderWithName:(NSString *)folderName
                onSuccess:(void(^)(NSDictionary *dict))success
                onFailure:(void(^)(NSError *error))failure
{
    NSString *query = [NSString stringWithFormat:@"SELECT Id FROM Folder WHERE Name = '%@'", folderName];
    SFRestRequest *request = [[SFRestAPI sharedInstance] requestForQuery:query];
    [[SFRestAPI sharedInstance] sendRESTRequest:request
                                      failBlock:^(NSError *e)
     {
         if (failure)
         {
             failure(e);
         }
     }
                                  completeBlock:^(id response)
     {
         if (success)
         {
             NSArray *records = [response objectForKey:@"records"];
             NSDictionary *dict = [records firstObject];
             success(dict);
         }
     }];
}

- (void)getAllUserAccountsOnSuccess:(void(^)(NSArray *accounts))success
                          onFailure:(void(^)(NSError *error))failure
{
    SFUserAccount *user = [[SFUserAccountManager sharedInstance] currentUser];
    NSString *query = [NSString stringWithFormat:@"SELECT Id, Name FROM Account WHERE OwnerId = '%@'", user.idData.userId];
    SFRestRequest * request = [[SFRestAPI sharedInstance] requestForQuery:query];
    [[SFRestAPI sharedInstance] sendRESTRequest:request
                                      failBlock:^(NSError *e)
     {
         if (failure)
         {
             failure(e);
         }
     }
                                  completeBlock:^(id response)
     {
         if (success)
         {
             NSArray *records = [response objectForKey:@"records"];
             success(records);
         }
     }];
}

- (void)getAllAccountsInOrganizationOnSuccess:(void(^)(NSArray *accounts))success
                                    onFailure:(void(^)(NSError *error))failure
{
    NSString *query = @"SELECT Id, Name, OwnerId FROM Account";
    SFRestRequest * request = [[SFRestAPI sharedInstance] requestForQuery:query];
    [[SFRestAPI sharedInstance] sendRESTRequest:request
                                      failBlock:^(NSError *e)
     {
         if (failure)
         {
             failure(e);
         }
     }
                                  completeBlock:^(id response)
     {
         if (success)
         {
             NSArray *records = [response objectForKey:@"records"];
             success(records);
         }
     }];
}

- (void)getAllContactsInOrganizationOnSuccess:(void(^)(NSArray *accounts))success
                                    onFailure:(void(^)(NSError *error))failure
{
    NSString *query = @"SELECT Id, FirstName, LastName, Email FROM Contact";
    SFRestRequest * request = [[SFRestAPI sharedInstance] requestForQuery:query];
    
    [[SFRestAPI sharedInstance] sendRESTRequest:request
                                      failBlock:^(NSError *e)
     {
         if (failure)
         {
             failure(e);
         }
     }
                                  completeBlock:^(id response)
     {
         if (success)
         {
             NSArray *records = [response objectForKey:@"records"];
             success(records);
         }
     }];
}

- (void)getContactsWithEmails:(NSArray *)emails
                    onSuccess:(void(^)(NSArray *contacts))success
                    onFailure:(void(^)(NSError *error))failure
{
    NSString *emailsString = [self getListString:emails];
    NSString * quesryString = [NSString stringWithFormat:@"SELECT Id, FirstName, LastName, Email FROM Contact WHERE Email IN (%@) ",emailsString];
    SFRestRequest * request = [[SFRestAPI sharedInstance] requestForQuery:quesryString];
    [[SFRestAPI sharedInstance]sendRESTRequest:request failBlock:^(NSError *e)
     {
         if (failure)
         {
             failure(e);
         }
     }
                                 completeBlock:^(id response)
     {
         if (success)
         {
             NSArray *records = [response objectForKey:@"records"];
             success(records);
         }
     }];
}

- (void)getContactsWithEmails:(NSArray *)emails
                   firstNames:(NSArray *)firstNames
                    lastNames:(NSArray *)lastNames
                    onSuccess:(void(^)(NSArray *contacts))success
                    onFailure:(void(^)(NSError *error))failure
{
    NSString *emailsString = [self getListString:emails];
    NSString *firstNamesString = [self getListString:firstNames];
    NSString *lastNamesString = [self getListString:lastNames];
    
    NSString * quesryString = [NSString stringWithFormat:@"SELECT Id, FirstName, LastName, Email FROM Contact WHERE Email IN (%@) OR FirstName in (%@) OR LastName IN (%@)",emailsString, firstNamesString, lastNamesString];
    SFRestRequest * request = [[SFRestAPI sharedInstance] requestForQuery:quesryString];
    [[SFRestAPI sharedInstance]sendRESTRequest:request failBlock:^(NSError *e)
     {
         if (failure)
         {
             failure(e);
         }
     }
                                 completeBlock:^(id response)
     {
         if (success)
         {
             NSArray *records = [response objectForKey:@"records"];
             success(records);
         }
     }];
}

- (void)getShareLinkForFile:(NSString *)fileId
                  onSuccess:(void(^)(NSString *shareLink))success
                  onFailure:(void(^)(NSError *error))failure
{
    SFRestRequest *request = [[SFRestAPI sharedInstance] requestForResources];
    NSString *path = [NSString stringWithFormat:@"/%@/connect/files/%@/file-shares/link", [SFRestAPI sharedInstance].apiVersion, fileId];
    request.path = path;
    request.method = SFRestMethodPUT;
    
    
    //-------------------------------//
    SFUserAccount *user = [[SFUserAccountManager sharedInstance] currentUser];
    NSLog(@"USER INFO");
    NSLog(@"CurrentUser is: %@", user);
    NSLog(@"CurrentUser [user.accessRestrictions] is: %d", user.accessRestrictions);
    NSLog(@"CurrentUser [user.isSessionValid] is: %d", user.isSessionValid);
    NSLog(@"CurrentUser [user.idData] is: %@", user.idData);
    NSLog(@"CurrentUser [user.idData.userType] is: %@", user.idData.userType);
    
    
    
    
    NSLog(@"--2.1-- GET Share Link Start");
    NSLog(@"Request is: %@", request);
    //-------------------------------//
    
    
    [[SFRestAPI sharedInstance] sendRESTRequest:request
                                      failBlock:^(NSError *e)
     {
         
         //-------------------------------//
         NSLog(@"--2.2-- GET Share Link Fail");
         NSLog(@"Error is: %@", e);
         NSLog(@"Error description is: %@", [e description]);
         //-------------------------------//
         
         if (failure)
         {
             failure(e);
         }
     }
                                  completeBlock:^(id response)
     {
         
         //-------------------------------//
         NSLog(@"--2.3-- GET Share Link Success");
         NSLog(@"Response is: %@", response);
         //-------------------------------//
         
         
         NSString *shareLink = response[@"fileViewUrl"];
         if (success)
         {
             success(shareLink);
         }
     }];
}

- (NSString *)getUserMail
{
    SFUserAccount *user = [[SFUserAccountManager sharedInstance] currentUser];
    return user.idData.email;
}


#pragma mark - POST methods

- (void)postContact:(NSDictionary *)contact
      toOpportunity:(NSString *)opportunityId
           delegate:(id<DRSalesforceInterfaceDelegate>)delegate
{
    self.delegate = delegate;
    NSMutableDictionary *contactDictionary = [contact mutableCopy];
    [contactDictionary setObject:opportunityId forKey:@"OpportunityId"];
    SFRestRequest *request = [[SFRestAPI sharedInstance] requestForCreateWithObjectType:@"OpportunityContactRole"
                                                                                 fields:contactDictionary];
    [[SFRestAPI sharedInstance] send:request delegate:nil];
}

- (void)postContact:(NSDictionary *)contact
      toOpportunity:(NSString *)opportunityId
          onSuccess:(void(^)(NSDictionary *dict))success
          onFailure:(void(^)(NSError *error))failure
{
    NSMutableDictionary *contactDictionary = [contact mutableCopy];
    [contactDictionary setObject:opportunityId forKey:@"OpportunityId"];
    [[SFRestAPI sharedInstance] performCreateWithObjectType:@"OpportunityContactRole"
                                                     fields:contactDictionary
                                                  failBlock:^(NSError *e)
     {
         if (failure)
         {
             failure(e);
         }
     }
                                              completeBlock:^(NSDictionary *dict)
     {
         if (success)
         {
             success(dict);
         }
     }];
}

- (void)postTask:(NSDictionary *)task
   toOpportunity:(NSString *)opportunityId
        delegate:(id<DRSalesforceInterfaceDelegate>)delegate
{
    self.delegate = delegate;
    NSMutableDictionary *taskDict = [task mutableCopy];
    [taskDict setObject:opportunityId forKey:@"WhatId"];
    SFRestRequest *request = [[SFRestAPI sharedInstance] requestForCreateWithObjectType:@"Task"
                                                                                 fields:taskDict];
    [[SFRestAPI sharedInstance] send:request delegate:self];
}

- (void)postTask:(NSDictionary *)task
   toOpportunity:(NSString *)opportunityId
       onSuccess:(void(^)(NSDictionary *dict))success
       onFailure:(void(^)(NSError *error))failure
{
    NSMutableDictionary *taskDict = [task mutableCopy];
    [taskDict setObject:opportunityId forKey:@"WhatId"];
    [[SFRestAPI sharedInstance] performCreateWithObjectType:@"Task"
                                                     fields:taskDict
                                                  failBlock:^(NSError *e)
     {
         if (failure)
         {
             failure(e);
         }
         
     }
                                              completeBlock:^(NSDictionary *dict)
     {
         if (success)
         {
             success(dict);
         }
     }];
}

- (void)postDocument:(NSDictionary *)document
            toFolder:(NSString *)folderId
           onSuccess:(void(^)(NSDictionary *dict))success
           onFailure:(void(^)(NSError *error))failure
{
    NSMutableDictionary *docDict = [document mutableCopy];
    [docDict setObject:folderId forKey:@"FolderId"];
    [[SFRestAPI sharedInstance] performCreateWithObjectType:@"Document"
                                                     fields:docDict
                                                  failBlock:^(NSError *e)
     {
         if (failure)
         {
             failure(e);
         }
     }
                                              completeBlock:^(NSDictionary *dict)
     {
         if (success)
         {
             success(dict);
         }
     }];
}

- (void)postAttachment:(NSDictionary *)attachment
         toOpportunity:(NSString *)opportunityId
             onSuccess:(void(^)(NSDictionary *dict))success
             onFailure:(void(^)(NSError *error))failure
{
    NSMutableDictionary *attDict = [attachment mutableCopy];
    [attDict setObject:opportunityId forKey:@"ParentId"];
    
    [[SFRestAPI sharedInstance] performCreateWithObjectType:@"Attachment"
                                                     fields:attDict
                                                  failBlock:^(NSError *e)
     {
         if (failure)
         {
             failure(e);
         }
     }
                                              completeBlock:^(NSDictionary *dict)
     {
         if (success)
         {
             success(dict);
         }
     }];
}

- (void)postPDFFile:(NSData *)fileData
           withName:(NSString *)name
        description:(NSString *)description
          onSuccess:(void(^)(id response))success
          onFailure:(void(^)(NSError *error))failure
{
    SFRestRequest *request = [[SFRestAPI sharedInstance] requestForUploadFile:fileData
                                                                         name:name
                                                                  description:description
                                                                     mimeType:@"application/pdf"];
    NSString *path = [NSString stringWithFormat:@"/%@/connect/files/users/me", [SFRestAPI sharedInstance].apiVersion];
    request.path = path;
    
    [[SFRestAPI sharedInstance] sendRESTRequest:request
                                      failBlock:^(NSError *e)
     {
         if (failure)
         {
             failure(e);
         }
     }
                                  completeBlock:^(id response)
     {
         if (success)
         {
             success(response);
         }
     }];
}

- (void)postTask:(NSDictionary *)task
     forContacts:(NSArray *)contacts
       onSuccess:(void(^)(id response))success
       onFailure:(void(^)(NSError *error))failure
{
    NSMutableDictionary *taskDict = [task mutableCopy];
    __block NSMutableDictionary *resultsDict = [[NSMutableDictionary alloc] init];
    __block NSUInteger counter = 0;
    
    for (NSInteger i = 0; i < [contacts count]; i++)
    {
        NSDictionary *contact = contacts[i];
        taskDict[@"WhoId"] = contact[@"Id"];
        taskDict[@"Status"] = @"Completed";
        NSString *contactEmail = contact[@"Email"];
        
        [[SFRestAPI sharedInstance] performCreateWithObjectType:@"Task"
                                                         fields:taskDict
                                                      failBlock:^(NSError *e)
         {
             if (failure)
             {
                 failure(e);
             }
         }
                                                  completeBlock:^(NSDictionary *dict)
         {
             counter++;
             [resultsDict setObject:dict[@"id"] forKey:contactEmail];
             
             if (counter >= [contacts count])
             {
                 success(resultsDict);
             }
         }];
    }
}

- (void)postOpenTask:(NSDictionary *)task
          forContact:(NSDictionary *)contact
           onSuccess:(void(^)(id response))success
           onFailure:(void(^)(NSError *error))failure
{
    NSMutableDictionary *taskDict = [task mutableCopy];
    
    taskDict[@"WhoId"] = contact[@"Id"];
    taskDict[@"Status"] = @"Not Started";
    taskDict[@"OwnerId"] = contact[@"Id"];
    
    __block NSMutableDictionary *resultsDict = [[NSMutableDictionary alloc] init];
    resultsDict[@"title"] = taskDict[@"Subject"];
    resultsDict[@"email"] = contact[@"Email"];
    
    [[SFRestAPI sharedInstance] performCreateWithObjectType:@"Task"
                                                     fields:taskDict
                                                  failBlock:^(NSError *e)
     {
         if (failure)
         {
             failure(e);
         }
     }
                                              completeBlock:^(NSDictionary *dict)
     {
         if (success)
         {
             resultsDict[@"id"] = dict[@"id"];
             success(resultsDict);
         }
     }];
}

- (void)postNoteToOpportunity:(NSString *)opportunityId
                    withTitle:(NSString *)noteTitle
                         text:(NSString *)noteText
                    onSuccess:(void(^)(id response))success
                    onFailure:(void(^)(NSError *error))failure
{
    NSMutableDictionary *noteDict = [[NSMutableDictionary alloc]init];
    noteDict[@"ParentId"] = opportunityId;
    noteDict[@"Title"] = noteTitle;
    noteDict[@"Body"] = noteText;
    
    [[SFRestAPI sharedInstance] performCreateWithObjectType:@"Note"
                                                     fields:noteDict
                                                  failBlock:^(NSError *e)
     {
         if (failure)
         {
             failure(e);
         }
     }
                                              completeBlock:^(NSDictionary *dict)
     {
         if (success)
         {
             success(dict);
         }
     }];
}

- (void)postChatterFeedWithQueryParameters:(NSDictionary *)queryParams
                                 onSuccess:(void(^)(id response))success
                                 onFailure:(void(^)(NSError *error))failure
{
    SFUserAccount *user = [[SFUserAccountManager sharedInstance] currentUser];
    SFRestRequest* request = [[SFRestAPI sharedInstance] requestForResources];
    request.path = [NSString stringWithFormat:@"/services/data/v24.0/chatter/feeds/user-profile/%@/feed-items", user.idData.userId];
    request.method = SFRestMethodPOST;
    request.queryParams = queryParams;
    
    [[SFRestAPI sharedInstance] sendRESTRequest:request
                                      failBlock:^(NSError *e)
     {
         if (failure)
         {
             failure(e);
         }
     }
                                  completeBlock:^(id response)
     {
         if (success)
         {
             success(response);
         }
     }];
}

- (void)postFileToChatter:(NSString *)fileId
                onSuccess:(void(^)(id response))success
                onFailure:(void(^)(NSError *error))failure
{
    NSMutableDictionary * postParams = [NSMutableDictionary new];
    
    NSMutableDictionary *attDict = [[NSMutableDictionary alloc]init];
    attDict[@"attachmentType"] = @"ExistingContent";
    attDict[@"contentDocumentId"] = fileId;
    
    postParams[@"attachment"] = attDict;
    
    SFUserAccount *user = [[SFUserAccountManager sharedInstance] currentUser];
    SFRestRequest* request = [[SFRestAPI sharedInstance] requestForResources];
    request.path = [NSString stringWithFormat:@"/services/data/v30.0/chatter/feeds/user-profile/%@/feed-items", user.idData.userId];
    request.method = SFRestMethodPOST;
    request.queryParams = postParams;
    
    [[SFRestAPI sharedInstance] sendRESTRequest:request
                                      failBlock:^(NSError *e)
     {
         if (failure)
         {
             failure(e);
         }
     }
                                  completeBlock:^(id response)
     {
         if (success)
         {
             success(response);
         }
     }];
}

#pragma mark - SFRestDelegate

- (void)request:(SFRestRequest *)request didLoadResponse:(id)jsonResponse
{
    NSArray *records = [jsonResponse objectForKey:@"records"];
    [self.delegate didLoadResponse:records];
}

- (void)request:(SFRestRequest*)request didFailLoadWithError:(NSError*)error
{
    [self.delegate didFailLoadWithError:error];
}

- (void)requestDidCancelLoad:(SFRestRequest *)request
{
    NSLog(@"requestDidCancelLoad: %@", request);
}

- (void)requestDidTimeout:(SFRestRequest *)request
{
    NSLog(@"requestDidTimeout: %@", request);
}

#pragma mark - Helpers

- (NSString *)getListString:(NSArray *)inputs
{
    NSString * result = @"";
    
    if ([inputs count] == 0)
    {
        return @"''";
    }
    else
    {
        for (int i = 0; i < [inputs count]; ++i)
        {
            result = [[[result stringByAppendingString:@"'"]
                       stringByAppendingString: [inputs objectAtIndex:i]]
                      stringByAppendingString:@"',"];
        }
        result = [result substringToIndex:result.length - 1];
    }
    return result;
}

@end
