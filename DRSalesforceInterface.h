//
//  DRSalesforceInterface.h
//  DRSalesforceInterface
//
//  Created by Plumb on 3/31/16.
//  Copyright © 2016 Recapify. All rights reserved.
//

#import <Foundation/Foundation.h>

/*!
 *  Lifecycle events for DRSalesforceInterface requests.
 */
@protocol DRSalesforceInterfaceDelegate <NSObject>

/*!
 *  Sent when a request has finished loading.
 *
 *  @param records The records from the response.
 */
- (void)didLoadResponse:(NSArray *)records;

/*!
 *  Sent when a request has failed due to an error.
 *
 *  @param error The error associated with the failed request.
 */
- (void)didFailLoadWithError:(NSError*)error;

@end



/*!
 *  Use this class for work with the Salesforce service.
 *  @discussion It contains API-methods
 */
@interface DRSalesforceInterface : NSObject

@property (weak, nonatomic) id <DRSalesforceInterfaceDelegate> delegate;

/*!
 *  Create singleton:
 *  @discussion Add '#import "DRSalesforceInterface.h"'
 *  @param Without parameters.
 *  @return Unique singleton object.
 *  @code [DRSalesforceInterface sharedInstance];
 *  @endcode
 */
+ (DRSalesforceInterface *)sharedInstance;

/*!
 *  Sends a request to the Salesforce server to get all user opportunities and invokes the appropriate delegate method.
 *  @param delegate The delegate object used when the response from the server is returned
 */
- (void)getUserOpportunitiesWithDelegate:(id<DRSalesforceInterfaceDelegate>)delegate;

/*!
 *  Sends a request to the Salesforce server to get all user opportunities
 *  @param success The block to be executed when the request successfully completes
 *  @param failure The block to be executed when the request fails
 */
- (void)getUserOpportunitiesOnSuccess:(void(^)(NSArray *opportunities))success
                            onFailure:(void(^)(NSError *error))failure;

/*!
 *  Sends a request to the Salesforce server to get all emails of contacts that related to the specified opportunity and invokes the appropriate delegate method
 *  @param opportunityId The opportunity identifier
 *  @param delegate      The delegate object used when the response from the server is returned
 */
- (void)getEmailsFromContactsRelatedToOpportunity:(NSString *)opportunityId
                                         delegate:(id<DRSalesforceInterfaceDelegate>)delegate;

/*!
 *  Sends a request to the Salesforce server to get all emails of contacts that related to the specified opportunity
 *  @param opportunityId The opportunity identifier
 *  @param success       The block to be executed when the request successfully completes
 *  @param failure       The block to be executed when the request fails
 */
- (void)getEmailsFromContactsRelatedToOpportunity:(NSString *)opportunityId
                                        onSuccess:(void(^)(NSArray *emails))success
                                        onFailure:(void(^)(NSError *error))failure;

/*!
 *  Sends a request to the Salesforce server to get user with the given email and invokes the appropriate delegate method
 *  @param email    The contact email
 *  @param delegate The delegate object used when the response from the server is returned
 */
- (void)getContactWithEmail:(NSString *)email
                   delegate:(id<DRSalesforceInterfaceDelegate>)delegate;

/*!
 *  Sends a request to the Salesforce server to get user with the given email
 *  @param email   The contact email
 *  @param success The block to be executed when the request successfully completes
 *  @param failure The block to be executed when the request fails
 */
- (void)getContactWithEmail:(NSString *)email
                  onSuccess:(void(^)(NSArray *contacts))success
                  onFailure:(void(^)(NSError *error))failure;

/*!
 *  Sends a request to the Salesforce server to get folder with the given folder name
 *
 *  @param folderName The name/title of the folder
 *  @param success    The block to be executed when the request successfully completes
 *  @param failure    The block to be executed when the request fails
 */
- (void)getFolderWithName:(NSString *)folderName
                onSuccess:(void(^)(NSDictionary *dict))success
                onFailure:(void(^)(NSError *error))failure;

/*!
 *  Sends a request to the Salesforce server to get all user accounts
 *
 *  @param success The block to be executed when the request successfully completes
 *  @param failure The block to be executed when the request fails
 */
- (void)getAllUserAccountsOnSuccess:(void(^)(NSArray *accounts))success
                          onFailure:(void(^)(NSError *error))failure;

/*!
 *  Sends a request to the Salesforce server to get all accounts in organization
 *
 *  @param success The block to be executed when the request successfully completes
 *  @param failure The block to be executed when the request fails
 */
- (void)getAllAccountsInOrganizationOnSuccess:(void(^)(NSArray *accounts))success
                                    onFailure:(void(^)(NSError *error))failure;

/*!
 *  Sends a request to the Salesforce server to get all contacts in organization
 *
 *  @param success The block to be executed when the request successfully completes
 *  @param failure The block to be executed when the request fails
 */
- (void)getAllContactsInOrganizationOnSuccess:(void(^)(NSArray *accounts))success
                                    onFailure:(void(^)(NSError *error))failure;

/*!
 *  Sends a request to the Salesforce server to get contacts with the given emails
 *
 *  @param emails  The list of emails
 *  @param success The block to be executed when the request successfully completes
 *  @param failure The block to be executed when the request fails
 */
- (void)getContactsWithEmails:(NSArray *)emails
                    onSuccess:(void(^)(NSArray *contacts))success
                    onFailure:(void(^)(NSError *error))failure;

/*!
 *  Sends a request to the Salesforce server to get contacts with the given emails, first names and last names
 *
 *  @param emails     The list of emails
 *  @param firstNames The list first names
 *  @param lastNames  The list of last names
 *  @param success    The block to be executed when the request successfully completes
 *  @param failure    The block to be executed when the request fails
 */
- (void)getContactsWithEmails:(NSArray *)emails
                   firstNames:(NSArray *)firstNames
                    lastNames:(NSArray *)lastNames
                    onSuccess:(void(^)(NSArray *contacts))success
                    onFailure:(void(^)(NSError *error))failure;

/*!
 *  Sends a request to the Salesforce server to get share link of the given file
 *
 *  @param fileId  The file identifier
 *  @param success The block to be executed when the request successfully completes
 *  @param failure The block to be executed when the request fails
 */
- (void)getShareLinkForFile:(NSString *)fileId
                  onSuccess:(void(^)(NSString *shareLink))success
                  onFailure:(void(^)(NSError *error))failure;

/*!
 *  This method returns user's email
 *
 *  @return The user's email
 */
- (NSString *)getUserMail;

/*!
 *  Sends a request to the Salesforce server to add contact to the given opportunity and invokes the appropriate delegate method
 *  @param contact       NSDictionary containing field names and values for the contact
 *  @param opportunityId The opportunity identifier
 *  @param delegate      The delegate object used when the response from the server is returned
 */
- (void)postContact:(NSDictionary *)contact
      toOpportunity:(NSString *)opportunityId
           delegate:(id<DRSalesforceInterfaceDelegate>)delegate;

/*!
 *  Sends a request to the Salesforce server to add contact to the given opportunity
 *
 *  @param contact       NSDictionary containing field names and values for the contact
 *  @param opportunityId The opportunity identifier
 *  @param success       The block to be executed when the request successfully completes
 *  @param failure       The block to be executed when the request fails
 */
- (void)postContact:(NSDictionary *)contact
      toOpportunity:(NSString *)opportunityId
          onSuccess:(void(^)(NSDictionary *dict))success
          onFailure:(void(^)(NSError *error))failure;

/*!
 *  Sends a request to the Salesforce server to add task to the given opportunity and invokes the appropriate delegate method
 *  @param task          NSDictionary containing field names and values for the task
 *  @param opportunityId The opportunity identifier
 *  @param delegate      The delegate object used when the response from the server is returned
 */
- (void)postTask:(NSDictionary *)task
   toOpportunity:(NSString *)opportunityId
        delegate:(id<DRSalesforceInterfaceDelegate>)delegate;

/*!
 *  Sends a request to the Salesforce server to add task to the given opportunity
 *
 *  @param task          NSDictionary containing field names and values for the task
 *  @param opportunityId The opportunity identifier
 *  @param success       The block to be executed when the request successfully completes
 *  @param failure       The block to be executed when the request fails
 */
- (void)postTask:(NSDictionary *)task
   toOpportunity:(NSString *)opportunityId
       onSuccess:(void(^)(NSDictionary *dict))success
       onFailure:(void(^)(NSError *error))failure;

/*!
 *  Sends a request to the Salesforce server to add document to the specified folder
 *
 *  @param document NSDictionary containing field names and values for the document
 *  @param folderId The folder identifier
 *  @param success  The block to be executed when the request successfully completes
 *  @param failure  The block to be executed when the request fails
 */
- (void)postDocument:(NSDictionary *)document
            toFolder:(NSString *)folderId
           onSuccess:(void(^)(NSDictionary *dict))success
           onFailure:(void(^)(NSError *error))failure;

/*!
 *  Sends a request to the Salesforce server to add attachment to the given opportunity
 *
 *  @param attachment    NSDictionary containing field names and values for the attachment
 *  @param opportunityId The opportunity identifier
 *  @param success       The block to be executed when the request successfully completes
 *  @param failure       The block to be executed when the request fails
 */
- (void)postAttachment:(NSDictionary *)attachment
         toOpportunity:(NSString *)opportunityId
             onSuccess:(void(^)(NSDictionary *dict))success
             onFailure:(void(^)(NSError *error))failure;

/*!
 *  Sends a request that can upload a new file to the Salesforce server
 *
 *  @param fileData    The Data to upload to the server
 *  @param name        The name/title of the file
 *  @param description A description of the file
 *  @param success     The block to be executed when the request successfully completes
 *  @param failure     The block to be executed when the request fails
 */
- (void)postPDFFile:(NSData *)fileData
           withName:(NSString *)name
        description:(NSString *)description
          onSuccess:(void(^)(id response))success
          onFailure:(void(^)(NSError *error))failure;

/*!
 *  Sends a request to the Salesforce server to add new tasks related to the given contacts
 *
 *  @param task       NSDictionary containing field names and values for the task
 *  @param contactsId NSArray containing contacts identifiers
 *  @param success    The block to be executed when the request successfully completes
 *  @param failure    The block to be executed when the request fails
 */
- (void)postTask:(NSDictionary *)task
     forContacts:(NSArray *)contacts
       onSuccess:(void(^)(id response))success
       onFailure:(void(^)(NSError *error))failure;

/*!
 *  Sends a request to the Salesforce server to add new open [Not Started] task to the given contact
 *
 *  @param task    NSDictionary containing field names and values for the task
 *  @param contact NSDictionary containing field names and values for the contact
 *  @param success The block to be executed when the request successfully completes
 *  @param failure The block to be executed when the request fails
 */
- (void)postOpenTask:(NSDictionary *)task
          forContact:(NSDictionary *)contact
           onSuccess:(void(^)(id response))success
           onFailure:(void(^)(NSError *error))failure;

/*!
 *  Sends a request to the Salesforce server to add new note to the given opportunity
 *
 *  @param opportunityId The opportunity identifier
 *  @param noteTitle     Note title
 *  @param noteText      Note text
 *  @param success       The block to be executed when the request successfully completes
 *  @param failure       The block to be executed when the request fails
 */
- (void)postNoteToOpportunity:(NSString *)opportunityId
                    withTitle:(NSString *)noteTitle
                         text:(NSString *)noteText
                    onSuccess:(void(^)(id response))success
                    onFailure:(void(^)(NSError *error))failure;

/*!
 *  Sends a request to the Salesforce server to add new chatter post
 *
 *  @param queryParams The parameters of the request
 *  @param success     The block to be executed when the request successfully completes
 *  @param failure     The block to be executed when the request fails
 */
- (void)postChatterFeedWithQueryParameters:(NSDictionary *)queryParams
                                 onSuccess:(void(^)(id response))success
                                 onFailure:(void(^)(NSError *error))failure;

/*!
 *  Sends a request to the Salesforce server to post file to the chatter feed
 *
 *  @param fileId  The file identifier
 *  @param success The block to be executed when the request successfully completes
 *  @param failure The block to be executed when the request fails
 */
- (void)postFileToChatter:(NSString *)fileId
                onSuccess:(void(^)(id response))success
                onFailure:(void(^)(NSError *error))failure;
@end
